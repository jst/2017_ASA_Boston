# Purpose

This repository has been created to provide supporting information for

```
Title: Geometry-based diffraction auralization for real-time applications in environmental noise

Conference: Acoustics '17 Boston ASA Meeting

Session: 2aAAa2

Date: Monday, June 26, 2017, 9:40 am - 10:00 am

Authors: Jonas Stienen and Michael Vorländer

```

Copyright Jonas Stienen, 2017. Media is [CC-BY 4.0](https://creativecommons.org/licenses/by/4.0/) and code is [Apache License Version 2.0](https://www.apache.org/licenses/LICENSE-2.0).
See also LICENSE files in the respective directories.


# C++ Code

See [ITADiffraction](https://git.rwth-aachen.de/ita/ITADiffraction), which is part of [ITAGeometricalAcoustics](https://git.rwth-aachen.de/ita/ITAGeometricalAcoustics) that requires [ITACoreLibs](https://git.rwth-aachen.de/ita/ITACoreLibs). It depends on different 3rd party projects such as the SkechUp API, OpenMesh and the ViSTA VR Toolkit.
A quick build guide can be obtained from the [Wiki](https://git.rwth-aachen.de/ita/ITACoreLibs/wikis/home) of ITACoreLibs.

The used versions of the code is linked as submodules in the `code` directory. Use CMake and the CMakeLists.txt in that folder to create a project with all tests and benchmarks.
Clone with `--recursive` flag in order to populate submodules.


# Support

Please understand that no support can be provided for building the code base, if the available guides are not sufficient.
The main purpose why the code is open source is to increase transparency and help other researchers to understand the approach and reproduce results (mostly by inspecting the source code).


# Further information

Find more about me and the Institute of Technical Acoustics (ITA) of the RWTH Aachen University on

[http://www.akustik.rwth-aachen.de](http://www.akustik.rwth-aachen.de) (website)

[http://blog.rwth-aachen.de/akustik](http://blog.rwth-aachen.de/akustik) (Akustik-blog)
